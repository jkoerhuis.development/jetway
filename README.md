<p style="text-align: center">
    <img src="jetway-logo.png" alt="Jetway logo" style="width: 100px" />
</p>

# Jetway
Current version `1.1.1`.

A utility package for Laravel that easily connects to a [Cockpit CMS](https://discourse.getcockpit.com/) installation.

## Requirements

Currently only supports `laravel/framework ^9.19`.

Requires basic knowledge of the [Cockpit CMS](https://discourse.getcockpit.com/).

## Installation

### Composer
Run ``composer install jesse-koerhuis/jetway``.

## Usage

### Configuration
Add the following data to the `env.php` file of your Laravel installation:

```dotenv
# Cockpit CMS configuration
CMS_BASE_URL="<Your Cockpit CMS base URL>"
CMS_ASSETS_BASE_URL="<Your Cockpit CMS base URL>/storage/uploads"

# Jetway configuration
COCKPIT_API_BASE_URL="<Your Cockpit CMS API base URL>"
COCKPIT_API_TOKEN="<Your Cockpit CMS API token>"
COCKPIT_CMS_PAGES_COLLECTION_NAME="<Your Cockpit CMS pages collection name>"
COCKPIT_CMS_PAGES_SLUG_FIELD_NAME="<Your Cockpit CMS page slug field name>"
```

### Dynamic Router
Locate the `routes/web.php` of your Laravel installation. Call the following code before all other routes:

```php
\JesseKoerhuis\Jetway\Http\Routing\DynamicRouter::prepare(); // Warning: Call this method only once!
\JesseKoerhuis\Jetway\Http\Routing\DynamicRouter::generate();
```

To add exceptional routes, use the following method below the generate method:

```php
\JesseKoerhuis\Jetway\Http\Routing\DynamicRouter::exception('/your_path', YourController::class);
```

### Fetching content models from the API
To fetch a content model with multiple entries: 
```php
\JesseKoerhuis\Jetway\Http\Api\ContentModel::get('MODEL_NAME');

// Or with filter and/or fields...
$data = \JesseKoerhuis\Jetway\Http\Api\ContentModel::get('MODEL_NAME', true, [
    // filter...
],
[
    // fields...
]);
```

To fetch a content model with only one entry:
```php
$data = \JesseKoerhuis\Jetway\Http\Api\ContentModel::get('MODEL_NAME', false);

// Or with custom parameters...
$data = \JesseKoerhuis\Jetway\Http\Api\ContentModel::get('MODEL_NAME', false, [
    // ...
]);
```

## Contributing
Jesse Koerhuis\
[jkoerhuis.development@gmail.com](mailto:jkoerhuis.development@gmail.com)

## License
This project is licensed under MIT. Feel free to clone this project or contribute to it by submitting a merge request.
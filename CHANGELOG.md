<p style="text-align: center">
    <img src="jetway-logo.png" alt="Jetway logo" style="width: 100px" />
</p>

# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [1.1.1] 05-02-2023

- Fixed a bug where the query parameters would not be sent to the API correctly.

## [1.1.0] 21-01-2023

- Added connector class ``JesseKoerhuis\Jetway\Http\Api\ContentModel`` to fetch content models from the Cockpit CMS.
- Changed usage of Php superglobal ``$_ENV`` to Laravel's ``env()`` function.

## [1.0.0] 31-12-2022

- Added dynamic router class ``JesseKoerhuis\Jetway\Http\Routing\DynamicRouter`` to automatically generate Laravel web routes based on a custom pages collection.
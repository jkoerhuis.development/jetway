<?php
/**
 * Copyright Jesse Koerhuis 2022.
 *
 * This code was written by Jesse Koerhuis.
 * For more information, please visit https://jessekoerhuis.nl/.
 */

declare(strict_types=1);

namespace JesseKoerhuis\Jetway\Config;

/**
 * Class Constants.
 *
 * @package JesseKoerhuis\Jetway\Config
 */
class Constants
{
    /**
     * @const string ENV_COCKPIT_API_BASE_URL
     */
    const ENV_COCKPIT_API_BASE_URL = 'COCKPIT_API_BASE_URL';

    /**
     * @const string ENV_COCKPIT_API_TOKEN
     */
    const ENV_COCKPIT_API_TOKEN = 'COCKPIT_API_TOKEN';

    /**
     * @const string ENV_COCKPIT_CMS_PAGES_COLLECTION_NAME
     */
    const ENV_COCKPIT_CMS_PAGES_COLLECTION_NAME = 'COCKPIT_CMS_PAGES_COLLECTION_NAME';

    /**
     * @const string ENV_COCKPIT_CMS_PAGES_SLUG_FIELD_NAME
     */
    const ENV_COCKPIT_CMS_PAGES_SLUG_FIELD_NAME = 'COCKPIT_CMS_PAGES_SLUG_FIELD_NAME';

    /**
     * @const string LARAVEL_CONTROLLER_MAIN_METHOD_NAME
     */
    const LARAVEL_CONTROLLER_MAIN_METHOD_NAME = 'index';
}

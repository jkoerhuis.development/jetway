<?php
/**
 * Copyright Jesse Koerhuis 2022.
 *
 * This code was written by Jesse Koerhuis.
 * For more information, please visit https://jessekoerhuis.nl/.
 */

declare(strict_types=1);

namespace JesseKoerhuis\Jetway\Http\Routing;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Route;
use JesseKoerhuis\Jetway\Config\Constants;

/**
 * Class DynamicRouter.
 *
 * @package JesseKoerhuis\Jetway\Http\Routing
 */
class DynamicRouter
{
    /**
     * @var array $_collectionStorage
     */
    public static array $_collectionStorage = [];

    /**
     * Fetch all slugs from a preferred collection and store it for later use.
     *
     * @author Jesse Koerhuis
     */
    public static function prepare(): void
    {
        $cockpitBaseUrl = env(Constants::ENV_COCKPIT_API_BASE_URL);
        $apiKey = env(Constants::ENV_COCKPIT_API_TOKEN);
        $pagesCollectionName = env(Constants::ENV_COCKPIT_CMS_PAGES_COLLECTION_NAME);
        $slugFieldName = env(Constants::ENV_COCKPIT_CMS_PAGES_SLUG_FIELD_NAME);

        $fields = [
            $slugFieldName => 1
        ];

        $headers = [
            'Content-Type' => 'application/json',
            'api-key' => $apiKey
        ];

        $formattedFields = json_encode($fields);
        $response = Http::withHeaders($headers)->get(
            "$cockpitBaseUrl/content/items/$pagesCollectionName?fields=$formattedFields"
        );

        if ($response->status() != Response::HTTP_OK) {
            Log::debug('Cockpit: Something went wrong while fetching data from API.');
        }

        $data = $response->json();

        if (isset($data['error'])) {
            Log::debug("Cockpit: {$data['error']}");
        }

        DynamicRouter::$_collectionStorage = $data ?? [];
    }

    /**
     * Generate routes from a preferred collection in the Cockpit CMS.
     *
     * @param string $controllerName
     * @param string|null $indexMethod
     * @author Jesse Koerhuis
     */
    public static function generate(string $controllerName, string $indexMethod = null): void
    {
        foreach (DynamicRouter::$_collectionStorage as $entry) {
            $slugFieldName = env(Constants::ENV_COCKPIT_CMS_PAGES_SLUG_FIELD_NAME);
            $action = [
                $controllerName,
                $indexMethod ?? Constants::LARAVEL_CONTROLLER_MAIN_METHOD_NAME
            ];

            if (!isset($entry[$slugFieldName])) continue;

            Route::get($entry[$slugFieldName], $action);
            Route::post($entry[$slugFieldName], $action);
        }
    }

    /**
     * Create an exceptional route from a preferred collection in the Cockpit CMS.
     *
     * @param string $slug
     * @param string $controllerName
     * @param string|null $httpMethod
     * @param string|null $indexMethod
     * @author Jesse Koerhuis
     */
    public static function exception(
        string $slug,
        string $controllerName,
        string $httpMethod = null,
        string $indexMethod = null,
    ): void {
        foreach (DynamicRouter::$_collectionStorage as $entry) {
            $method = $httpMethod ?? Request::METHOD_GET;
            $slugFieldName = env(Constants::ENV_COCKPIT_CMS_PAGES_SLUG_FIELD_NAME);

            $action = [
                $controllerName,
                $indexMethod ?? Constants::LARAVEL_CONTROLLER_MAIN_METHOD_NAME
            ];

            if (!isset($entry[$slugFieldName])) continue;

            switch (strtoupper($method)) {
                case Request::METHOD_GET:
                    Route::get($slug, $action);
                    break;

                case Request::METHOD_POST:
                    Route::post($slug, $action);
                    break;

                default:
                    Route::get($slug, $action);
            }
        }
    }
}

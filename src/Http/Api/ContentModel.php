<?php
/**
 * Copyright Jesse Koerhuis 2022.
 *
 * This code was written by Jesse Koerhuis.
 * For more information, please visit https://jessekoerhuis.nl/.
 */

declare(strict_types=1);

namespace JesseKoerhuis\Jetway\Http\Api;

use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Log;
use JesseKoerhuis\Jetway\Config\Constants;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class ContentModel.
 *
 * @package JesseKoerhuis\Jetway\Http\Api
 */
class ContentModel
{
    /**
     * Get a content model from the Cockpit API.
     *
     * @param string $modelName
     * @param bool $multiple
     * @param array $filter
     * @param array $fields
     * @return array
     * @author Jesse Koerhuis
     */
    public static function get(
        string $modelName,
        bool $multiple = true,
        array $filter = [],
        array $fields = []
    ): array {
        $cockpitBaseUrl = env(Constants::ENV_COCKPIT_API_BASE_URL);

        $headers = [
            'Content-Type' => 'application/json',
            'api-key' => env(Constants::ENV_COCKPIT_API_TOKEN)
        ];

        $countPath = $multiple ? 'items' : 'item';
        $formattedFilter = json_encode($filter);
        $formattedFields = json_encode($fields);

        $response = Http::withHeaders($headers)
            ->get("$cockpitBaseUrl/content/$countPath/$modelName?filter=$formattedFilter&fields=$formattedFields");

        if ($response->status() != Response::HTTP_OK) {
            Log::debug('Cockpit: Something went wrong while fetching model from API.');
            return [];
        }

        $data = $response->json();

        if (isset($data['error'])) {
            Log::debug("Cockpit: {$data['error']}");
            return [];
        }

        return $data;
    }
}
